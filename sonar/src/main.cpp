#include <Arduino.h>

#define TRUE 1
#define FALSE 0
#define SOUND_VEL 0.034

int insertionSortMedian(int *array, int size);
int checkHistory(int *history, int size, int dist, int threshold);
void zfill(int *array, int size);
void append(int *array, int size, int value);

const int b = 10;
const int h = 200;

// Auxiliary arrays to filter sonar data
int burst[b];
int history[h];

// Pin numbers
const int trigPin = D3;
const int echoPin = D2;
const int ledPin = D7;

// Variables
int countBurst;
long duration;
int distance;
int median;

void setup() {

  /*Fill arrays burst and history with zeros*/
  zfill(burst, b);
  zfill(history, h);
  countBurst = 0;

  /* Set pin modes */
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  pinMode(ledPin, OUTPUT);

  /*Start serial communication */
  Serial.begin(9600);
}

void loop() {
  // Clears the trigPin
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);

  // Sets the trigPin on HIGH state for 10 micro seconds
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);

  // Reads the echoPin, returns the sound wave travel time in microseconds
  duration = pulseIn(echoPin, HIGH);

  /* Calculates distance: duration is in microseconds and we
  assume sound waves propagate in air with velocity 340 m/s = 0.034 cm/us */
  distance = duration*SOUND_VEL/2;


  if (countBurst < 10) {
    burst[countBurst] = distance;
    countBurst++;
  }
  else {
    //Get the median of b sonar reads
    median = insertionSortMedian(burst, b);

    //Create history of sonar read medians
    append(history, h, median);

    //Check user proximity.
    if (checkHistory(history, h, 30, 0.9*h)) {
      digitalWrite(ledPin, HIGH);
    } else {
      digitalWrite(ledPin, LOW);
    }

    countBurst = 0;
  }
}


/* Appends value to beginning of array */
void append(int *array, int size, int value) {
  for (int i = size; i > 0; i--) {
    array[i] = array[i-1];
  }
  array[0] = value;
}

/* Fill array with zeros */
void zfill(int *array, int size) {
  for (int i = 0; i < size; i++) {
    array[i] = 0;
  }
}

// Returns true if history is less than or equal to dist at least threshold times, otherwise returns 0.
int checkHistory(int *history, int size, int dist, int threshold) {
  int i, count;

  if (threshold > size) {
    Serial.println("Invalid threshold value. History size must be greater than threshold.");
    return FALSE;
  }

  count = 0;
  for (i = 0; i < size; i++) {
    if (history[i] <= dist) {
      count++;
    }
  }
  Serial.print("Count: ");
  Serial.println(count);
  return count >= threshold ? TRUE: FALSE;
}

int insertionSortMedian(int *array, int size) {
  int i, j, key;

  for (i = 1; i < size; i++) {
    j = i-1;
    key = array[i];
    while (key < array[j] && j >= 0) {
      array[j+1] = array[j];
      j--;
    }
    array[j+1] = key;
  }
  return array[size/2-1];
}

import cv2

if __name__ == "__main__":
    img = cv2.imread("images/blue.jpg")

    cv2.namedWindow("window", cv2.WND_PROP_FULLSCREEN)
    cv2.setWindowProperty("window",cv2.WND_PROP_FULLSCREEN,cv2.WINDOW_FULLSCREEN)
    print(img.shape)
    cv2.imshow("window", img)

    if cv2.waitKey(0) == ord(q):
        exit(1)

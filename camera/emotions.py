import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from io import BytesIO
from PIL import Image
import requests

subscription_key = "6d3eb2b5e73f4bcc969da9500b8a6c1f"
assert subscription_key

# emotion_recognition_url = "https://westus.api.cognitive.microsoft.com/emotion/v1.0/recognize"

emotion_recognition_url = "https://westcentralus.api.cognitive.microsoft.com/face/v1.0"

header = {'Ocp-Apim-Subscription-Key': subscription_key }


def annotate_image(image_path):
    image_data = open(image_path, "rb").read()
    headers  = {'Ocp-Apim-Subscription-Key': subscription_key, "Content-Type": "application/octet-stream" }
    response = requests.post(emotion_recognition_url, headers=headers, data=image_data)
    response.raise_for_status()
    analysis = response.json()

    plt.figure()

    image  = Image.open(image_path)
    ax     = plt.imshow(image, alpha=0.6)

    for face in analysis:
        fr = face["faceRectangle"]
        em = face["scores"]
        origin = (fr["left"], fr["top"])
        p = Rectangle(origin, fr["width"], fr["height"], fill=False, linewidth=2, color='b')
        ax.axes.add_patch(p)
        ct = "\n".join(["{0:<10s}{1:>.4f}".format(k,v) for k, v in sorted(list(em.items()),key=lambda r: r[1], reverse=True)][:3])
        plt.text(origin[0], origin[1], ct, fontsize=20, va="bottom")
    _ = plt.axis("off")


if __name__ == "__main__":
    annotate_image("images/emotion_1.jpg")
